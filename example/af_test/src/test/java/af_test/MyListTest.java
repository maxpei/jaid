package af_test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyListTest {
    MyList myList;

    @Before
    public void before() throws Exception {
        myList = new MyList();
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testDuplicateNotEmpty() throws Exception {
        myList.extend("a");
        myList.extend("b");
        myList.extend("c");
        myList.extend("d");
        MyList dupList = myList.duplicate(3);
        Assert.assertEquals(3, dupList.count());
    }

    @Test
    public void testDuplicateEmpty() throws Exception {
        MyList dupList = myList.duplicate(2);
        Assert.assertEquals(0, dupList.count());
    }

}
