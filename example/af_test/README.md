# Example for JAID

This is a man-made buggy program for testing Jaid.

# How To
1. Edit the value of `ProjectRootDir` variable in  `af_test.properties`(for Windows system) or `mac_project.properties` (for Mac or Linux system) to match your machine environment.

2. Pass the properties file as program arguments by edit the `Run/Debug Configuration`. Refer to the following image to config the configuration

![how_to_config](../../doc/img/how_to_config.png)