# JAID
This is the implementation of Jaid.

### Configuring JAID project in IntelliJ IDEA

1. Clone JAID project from https://bitbucket.org/maxpei/fixja.git to a local directory $JAID_DIR$.

2. Open JAID project in your IntelliJ IDEA IDE. Go to \[File\]/\[Open\], then browse to $JAID_DIR$ and select `jaid.iml` and click on OK. Or import the provided module file `jaid.iml` in \[Modules\] of project structure after open the project.

    ![import_modules](doc/img/import_modules.png)

3. The JAID project assume a JDK of version 1.8. You need to set up the SDK, language level, compiler output at \[File\]/\[Project Structures\]/\[Project Settings\]/\[Project\].

    ![project_structure](doc/img/project_structure.png)

4. Before executing JAID, you may need to prepare a buggy program for JAID to fix, see how to [prepare subject faults](https://bitbucket.org/maxpei/jaid/wiki/script_manual.md).
    * The input of JAID includes a buggy program with a test suit contains at least one failing test to reveal the bug, and the method where the bug reside in. The above information alone with other configurations should be specified in a properties file (please refer to Command Line Arguments for details of each argument) and config as a running argument of JAID as the following image. (The most important configuration is specifying the properties file in the program argument `--JaidSettingFile`.)

    ![How_to_config](doc/img/how_to_config.png)

    * Note: The JAID project comes with a simple example project for demonstration in folder $JAID_DIR$/example. Please refer to the [Example](example/af_test/README.md).

### Read more:

*  [Wiki](https://bitbucket.org/maxpei/jaid/wiki/Home).
*  [ASE 2017](http://ase2017.org/) paper: Liushan Chen, Yu Pei, and Carlo A. Furia. Contract-based Program Repair Without the Contracts. In Proceedings of the 32nd IEEE/ACM International Conference on Automated Software Engineering (ASE). ([preprint](https://bitbucket.org/maxpei/jaid/downloads/ASE2017_JAID_preprint.pdf))

## Copyright and License

JAID is distributed under the terms of the [GNU General Public License](http://www.gnu.org/licenses/gpl.html). There is absolutely NO WARRANTY for JAID, its code and its documentation.

